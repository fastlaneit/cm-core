package com.fastlaneit.chainmail.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@SpringBootApplication
public class Application {

    public static final String V1 = "/v1";

    public static final String DEV = "dev";
    public static final String QA = "qa";


    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
