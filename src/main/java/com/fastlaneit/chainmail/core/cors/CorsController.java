package com.fastlaneit.chainmail.core.cors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.fastlaneit.chainmail.core.Application.V1;

/**
 * Created by egorkolesnikov on 21/05/2016.
 */
@RestController
public class CorsController {

    private static final Logger log = LoggerFactory.getLogger("Lalalalala");

    @Autowired
    private ApplicationContext context;

    @Autowired
    RequestMappingHandlerMapping requestMappingHandlerMapping;

    @RequestMapping(value = V1 + "/cors", method = RequestMethod.POST)
    public void cors() {
        log.error("Successful class reloading!");
        requestMappingHandlerMapping.setCorsConfigurations(Collections.emptyMap());
    }

    @RequestMapping(value = V1 + "/sso", method = RequestMethod.GET)
    public Map<String, String> sso(HttpServletRequest request, HttpServletResponse response) {
        log.error("SSO starting");
        Cookie[] cookies = request.getCookies();
        if(cookies == null) {
            return Collections.EMPTY_MAP;
        }
        response.addCookie(new Cookie("SSO", "1231231231232"));

        Map <String, Cookie> cookieMap = Arrays.stream(cookies).collect(Collectors.toMap(Cookie::getName, c -> c));
        Cookie sso = cookieMap.get("SSO");
        if(sso != null) {
            return Collections.unmodifiableMap(Stream.of(
                    new AbstractMap.SimpleEntry<String, String>("user", "username"),
                    new AbstractMap.SimpleEntry<String, String>("token", "abcdefg")
            ) .collect(Collectors.toMap((e) -> e.getKey(), (e) -> e.getValue()))) ;
        }
        return Collections.EMPTY_MAP;

    }

    @RequestMapping(value = V1 + "/login", method = RequestMethod.GET)
    public void login(HttpServletRequest request, HttpServletResponse response) {
        log.error("LOGIN starting");
        response.addCookie(new Cookie("SSO", "1231231231232"));
    }
}
