package com.fastlaneit.chainmail.core.user;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * Created by egorkolesnikov on 21/05/2016.
 */
@Builder
@AllArgsConstructor
@Getter
@EqualsAndHashCode
public class User {

    private final Long id;
    private final String name;
    private final String key;

    public static User fromEntity(final UserEntity userEntity) {
        return new User(userEntity.getId(), userEntity.getName(), userEntity.getKey());
    }

}
