package com.fastlaneit.chainmail.core.user;

import com.fastlaneit.chainmail.core.common.data.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Builder
@Table(name = "users")
public class UserEntity extends BaseEntity {

    @Id
    @NotNull
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String key;

    public static UserEntity from(final User user) {
        return new UserEntity(user.getId(), user.getName(), user.getKey());
    }
}
