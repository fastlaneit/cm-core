package com.fastlaneit.chainmail.core.user;

import org.springframework.data.repository.CrudRepository;

/**
 * Created by egorkolesnikov on 21/05/2016.
 */
public interface UserRepository extends CrudRepository<UserEntity, Long> {

}
