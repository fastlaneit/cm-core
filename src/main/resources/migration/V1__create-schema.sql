create table USERS (
    id bigint not null,
    name varchar(255) not null,
    key varchar(255) not null,
    createdDate timestamp,
    primary key (id)
);
